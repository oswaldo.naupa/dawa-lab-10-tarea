const express = require('express');
const Peliculas = require('../models/peliculas');
const Noticias = require('../models/noticias');
const Usuario = require('../models/usuario');
const router = express.Router();
const {verificaToken} = require("../middlewares/authentication")


if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }


router.get("/pelicula", async function (req, res) {
    const peliculas = await Peliculas.find({});
    res.render('pelicula', { peliculas })
});
router.get("/pelicula/categoria/:categoria", async function (req, res) {
    const categoria = req.params.categoria
    const peliculas = await Peliculas.find({categoria:req.params.categoria});
    res.render('pelicula', {peliculas: peliculas, categoria:categoria})
});
router.get('/agregarPelicula/',verificaToken, async function(req, res){
    let error = [];
    res.render('agregarPelicula', {error})
});
router.post('/pelicula_crear',verificaToken, async function(req, res) {
    const { titulo, descripcion, image, categoria, tag, tag1, tag2 } = req.body;
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    let newPelicula = new Peliculas({ titulo, descripcion, image, categoria, tags });
    await newPelicula.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/pelicula");
});
router.get("/pelicula/:id", async function (req, res) {
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('pelicula-single', {pelicula: pelicula})
});
router.post('/pelicula/:id/comentar',  async function(req, res) {
    const { nombre_usuario, comentario } = req.body;
    const noticia = await Peliculas.findById(req.params.id);
    let img_usuario = 'usuario.png';
    noticia.comentarios.push({ nombre_usuario, img_usuario, comentario });
    await noticia.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/pelicula/" + req.params.id);
});


router.get('/editPelicula/:id',verificaToken, async function(req, res){
    let error = [];
    const usersName = await Usuario.find({});
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('editPelicula', {error, pelicula, usersName })
});
router.put('/updatepelicula/:id',verificaToken, async function(req, res){
    const { titulo,descripcion,image,categoria,tag,tag1,tag2 } = req.body;
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    await Peliculas.findByIdAndUpdate(req.params.id, { titulo: titulo,descripcion: descripcion, image: image,categoria:categoria,tags:tags });
    res.redirect("/pelicula");
})
router.delete('/deletePelicula/:id',verificaToken, async function(req, res) {
    let id = req.params.id;
    await Peliculas.findByIdAndDelete(id);
    res.redirect("/pelicula");
});
module.exports = router