require('./config/config')
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const routesIndex = require('./routes/routesIndex');
const routesUser = require('./routes/routesUser');
const routesPelicula = require('./routes/routesPeliculas');
const methodOverride = require('method-override');

//lectura de cookie
var cookieParser = require('cookie-parser');
app.use(cookieParser());

//local
if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}


const path = require('path');

app.use(express.json());
app.use(express.urlencoded());
//uso de variable local
app.use((req, res, next)=>{
  global.user = req.cookies.token || null;
  next();
});

mongoose.connect(process.env.URLDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }, (err, des) =>{
    if(err) throw err;
    console.log('Base de datos online')
});

app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');
app.use(methodOverride('_method'));
app.listen(process.env.PORT, () => console.log(`Escuchando puerto ${process.env.PORT}`));
//Primer ruta para la pag
app.use(routesIndex);
//Segunda ruta para ver si se hace un login
app.use(routesUser);
//Ruta para controlar el crud del peliculas
app.use(routesPelicula);

app.use(function(req, res, next) {
    res.status(404).render('404');
});
